//File:         Triangle.h
//Author(s):    Jonathan Zallas
//Created on:   January 28, 2014
//Purpose:      Creates a triangle structure such that each row has
//              one number more than the previous row

//Notes: While this problem can be considered recursively, it will take too long to solve
//       Instead, minimizing the triangle from the bottom to the top will take less steps
//       so I create a triangle structure in memory and slowly made it smaller until there
//       is only one number left: the largest sum.

// ******************PUBLIC OPERATIONS*********************
// int getRows( )           --> Returns number of rows
// long getLargestSum( )    --> Return largest sum out of all paths
// Triangle(char* filename) --> Constructs a triangle structure with data from 'filename'

#ifndef TRIANGLE_H_INCLUDED
#define TRIANGLE_H_INCLUDED

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>

using namespace std;

class Triangle
{
private:
    vector<vector<long> > triangle;
    int rows;
    void initializeTriangle(char* filename);
    void replaceWithLargestSum(int row, int index);
    void replaceRow(int row);
    void minimizeTriangle();

public:
    Triangle(char* filename);
    long getLargestSum();
};

#endif // TRIANGLE_H_INCLUDED
