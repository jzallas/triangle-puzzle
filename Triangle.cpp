//File:         Triangle.cpp
//Author(s):    Jonathan Zallas
//Created on:   January 28, 2014
//Purpose:      Creates a triangle structure such that each row has
//              one number more than the previous row


#include "Triangle.h"

void Triangle::replaceWithLargestSum(int currentRow, int currentIndex)
{
    //if there are numbers below this number
    if (currentRow < rows-1)
    {
        //get the numbers to the left and right
        int left = triangle[currentRow+1][currentIndex];
        int right = triangle[currentRow+1][currentIndex+1];
        //replace this number with the sum of this number plus the bigger of the two numbers below it
        triangle[currentRow][currentIndex] = triangle[currentRow][currentIndex] + (left > right? left: right);
    }
}

void Triangle::replaceRow(int currentRow)
{

    for (unsigned int i = 0; i < triangle[currentRow].size(); i++)
        replaceWithLargestSum(currentRow, i);

}

void Triangle::minimizeTriangle()
{

    //while theres more than one row
    while(rows > 1)
    {
        //replace the second to last row
        replaceRow(rows-2);
        //resize the triangle since last row no longer needed
        triangle.resize(rows-1);
        //update rows;
        rows--;
    }
}

void Triangle::initializeTriangle(char* filename)
{
    ifstream read(filename);

    string line;
    while(!read.eof()) // while stream has data to read
    {
        getline(read, line); // get 1 line
        rows++;
        stringstream ss; // stream for getline
        ss << line;

        while(!ss.eof()) // while stream has data to read
        {
            string number;
            ss >> number; // get one number


            if(!number.empty())
            {

                if (triangle.size()< rows)
                    triangle.resize(rows);
                triangle[rows-1].push_back(atol(number.c_str()));
            }
        }
    }
    read.close();
}

Triangle::Triangle(char* filename)
{
    rows = 0;
    initializeTriangle(filename);
}

long Triangle::getLargestSum()
{
    minimizeTriangle();
    return triangle[0][0];
}
