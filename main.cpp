//File:         main.cpp
//Author(s):    Jonathan Zallas
//Created on:   January 28, 2014
//Description:  Outputs the sum of the path with the largest sum
//Usage:        Runs in terminal using <programname> <file1> <file2> <flag>
//Version:      1.0
//

#include <iostream>
#include "Triangle.h"

using namespace std;

int main(int argc, char **argv)
{
    //Check input arguments
    if (argc!=2)
    {
        cout << "Usage: " << argv[0] << " <triangle file>" << endl;

        return 0;
    }

    Triangle* triangle = new Triangle(argv[1]);

    cout << triangle->getLargestSum() << endl;

    delete triangle;
    triangle = NULL;

    return 0;
}

